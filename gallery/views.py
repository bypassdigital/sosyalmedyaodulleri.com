from django.views import generic

from gallery.models import Image


class GalleryView(generic.ListView):
    template_name = "gallery.html"
    model = Image
    context_object_name = "gallery"
