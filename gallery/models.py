from django.db import models
from django.utils.translation import gettext as _


class Image(models.Model):
    name = models.CharField(_("Name"), max_length=255)
    file = models.ImageField(_("Image"), upload_to="gallery")

    class Meta:
        verbose_name = _("Image")
        verbose_name_plural = _("Images")

    def __str__(self):
        return self.name
