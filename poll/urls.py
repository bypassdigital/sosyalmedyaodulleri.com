from django.urls import path

from poll import views

app_name = "poll"


urlpatterns = [
    path('<int:pk>/', views.PollStartView.as_view(), name="start"),
    path('<int:pk>/survey/', views.PollSurveyView.as_view(), name="survey"),
]
