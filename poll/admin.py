from django.contrib import admin

from poll.models import Poll, Participant


admin.site.register(Poll)
admin.site.register(Participant)
