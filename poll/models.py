from django.db import models
from django.urls import reverse
from django.utils.translation import gettext as _


class Poll(models.Model):
    name = models.CharField(_("Name"), max_length=255)
    gd_url = models.URLField(_("Survey URL"))
    active = models.BooleanField(_("Active"), default=True)
    desc = models.TextField(_("Description"))

    class Meta:
        verbose_name = _("Poll")
        verbose_name_plural = _("Polls")

    def __str__(self):
        return self.name


class Participant(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    name = models.CharField(_("Full Name"), max_length=255)
    email = models.EmailField(_("Email"))
    student_id = models.PositiveSmallIntegerField(_("Student ID"), max_length=9)

    class Meta:
        verbose_name = _("Participant")
        verbose_name_plural = _("Participants")
        unique_together = [['poll', 'email'], ['poll', 'student_id'], ['email', 'student_id']]

    def __str__(self):
        return self.name
