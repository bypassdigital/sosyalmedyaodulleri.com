from urllib.parse import splitquery

from django.views import generic
from django.shortcuts import redirect

from poll.models import Poll, Participant
from poll.forms import ParticipantForm, ParticipantNewForm


class PollStartView(generic.FormView):
    template_name = "poll_start.html"
    form_class = ParticipantNewForm

    def get_success_url(self, *args, **kwargs):
        return "/poll/" + str(self.kwargs['pk']) + "/survey/"


class PollSurveyView(generic.DetailView):
    template_name = "poll_survey.html"
    model = Poll

    def get_context_data(self, *args, **kwargs):
        context = super(PollSurveyView, self).get_context_data(*args, **kwargs)
        obj = self.get_object()
        try:
            context['gd_url'] = splitquery(obj.gd_url)[0]
        except Exception:
            context['gd_url'] = obj.gd_url
        return context
