from django import forms
from coresite.forms import FormAbstract
from django.core.validators import EmailValidator, MaxValueValidator, MinValueValidator

from poll.models import Participant


class ParticipantForm(FormAbstract):
    class Meta:
        model = Participant
        fields = ['name', 'email', 'student_id']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].validators += [EmailValidator(whitelist=['itu.edu.tr'])]
        self.fields['student_id'].widget.attrs['max'] = 999999999


class ParticipantNewForm(forms.Form):
    name = forms.CharField(label="Ad-Soyad")
    email = forms.EmailField(label="ITU Eposta", validators=[EmailValidator(whitelist=['itu.edu.tr'])])
    student_id = forms.IntegerField(label="Öğrenci Numarası", max_value=999999999)

    def clean(self):
        cleaned_data = super().clean()
        email = cleaned_data.get("email")
        domain = email.split("@")[1]
        if "itu" in domain:
            return cleaned_data
        else:
            raise forms.ValidationError("Geçersiz Email")

