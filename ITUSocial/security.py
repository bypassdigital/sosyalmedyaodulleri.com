import os

"""
This file is a template file for security.py. Without that file your site can't work.

You sould create a copy of this file named security.py and fill empty fields on that file.

security.py file is in the .gitignore list, so it wont be affected by any git action.

This file is important for different deployment
"""

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Debug mode, if your deployment is a development environment, keep it True. If it's not make it False
DEBUG = True

# Unique key for each deployment. Before creating security.py fill here with a randomly generated key
SECRET_KEY = 'Not So Secret'

# Allowed host names. Your deployment can only be acceced by those domains and IP addresses.
ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

# Your deployments database connection. For development keep it like this.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
