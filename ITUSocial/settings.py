import os
from . import security

BASE_DIR = security.BASE_DIR

SECRET_KEY = security.SECRET_KEY

DEBUG = security.DEBUG

ALLOWED_HOSTS = security.ALLOWED_HOSTS

DATABASES = security.DATABASES

SITE_ID = 1

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.sitemaps',

    'sorl.thumbnail',
    'ckeditor',
    'ckeditor_uploader',
    'mptt',
    'debug_toolbar',
    'bootstrapform',

    'coresite.apps.CoresiteConfig',
    'pages.apps.PagesConfig',
    'poll.apps.PollConfig',
    'gallery.apps.GalleryConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

ROOT_URLCONF = 'ITUSocial.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': DEBUG,
        },
    },
]

INTERNAL_IPS = ['127.0.0.1']

WSGI_APPLICATION = 'ITUSocial.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization

LANGUAGE_CODE = 'tr-tr'

TIME_ZONE = 'Europe/Istanbul'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = [os.path.join(BASE_DIR, 'locale')]

# Paths
STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# ckeditor
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'full',
        'embed_provider': '//iframe.ly/api/oembed?url={url}&callback={callback}&api_key=0a84c29c860a06e839b481',
        'allowedContent': True,
        'extraAllowedContent': 'p(*)[*]{*}; \
            div(*)[*]{*}; \
            li(*)[*]{*}; \
            ul(*)[*]{*}; \
            iframe(*)[*]{*}; \
            button(*)[*]{*}; \
            span(*)[*]{*}; \
            img(*)[*]{*}; \
            a(*)[*]{*}; \
            script(*)[*]{*}',
        'extraPlugins': ','.join([
            'uploadimage',
            'uploadwidget',
            'div',
            'autolink',
            'embed',
            'autogrow',
            'embed',
            'widget',
            'widgetselection',
            'lineutils',
            'clipboard',
            'dialog',
            'dialogui',
            'elementspath',
            'preview',
        ]),
    },
}

CKEDITOR_RESTRICT_BY_USER = False
CKEDITOR_BROWSE_SHOW_DIRS = True
CKEDITOR_IMAGE_BACKEND = "pillow"

CKEDITOR_UPLOAD_SLUGIFY_FILENAME = True
CKEDITOR_JQUERY_URL = 'http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_ALLOW_NONIMAGE_FILES = True
AWS_QUERYSTRING_AUTH = False

# Site Settings
SOCIAL_CHOICES = (
    ("fab fa-facebook", "Facebook"),
    ("fab fa-instagram", "Instagram"),
)

HAS_CONTACT = True

INDEX_LIST = False
