# Generated by Django 2.1.2 on 2018-10-22 20:00

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Title')),
                ('url', models.SlugField(max_length=220, verbose_name='URL')),
                ('thumbnail', models.ImageField(upload_to='thumbnails/%Y/%m', verbose_name='Thumbnail')),
                ('keywords', models.CharField(blank=True, max_length=1000, null=True, verbose_name='Keywords')),
                ('body', ckeditor_uploader.fields.RichTextUploadingField(verbose_name='Content')),
            ],
            options={
                'verbose_name': 'Page',
                'verbose_name_plural': 'Pages',
            },
        ),
    ]
