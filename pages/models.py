from django.db import models
from django.urls import reverse
from django.utils.translation import gettext as _

from coresite.models import ContentBase


class Page(ContentBase):
    
    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")

    def get_absolute_url(self):
        return reverse('pages:page', args=[str(self.url)])
