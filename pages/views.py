from django.views import generic
from .models import Page


class PageDetailView(generic.DetailView):
    model = Page
    context_object_name = "page"
    slug_field = "url"
    template_name = "page.html"
